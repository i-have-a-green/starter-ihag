<?php
/**
 * Block Name: Cards
 */
 ?>

<!-- <div>Qui sommes-nous ?</div> -->

<div class="wrapper-company alignfull">
    <div class="alignwide card-wrapper">
            <?php

        // Check rows exists.
        if( have_rows('cards') ):

            // Loop through rows.
            while( have_rows('cards') ) : the_row();
            ?>
            <div class="card-solution">
                <p style="color:<?php the_sub_field('color'); ?>">
                    <?php
                        the_sub_field('icone');
                    ?>
<!--                     <span style="background-color:<?php the_sub_field('color'); ?>"></span> -->
                </p>
                <h2><?php the_sub_field('title'); ?></h2>
                <?php
                    the_sub_field('content');
                ?>
            </div>
            <?php
                // Load sub field value.
                
                // Do something...

            // End loop.
            endwhile;

        // No value.
        else :
            // Do something...
            echo "Veuillez renseigner ce bloc";
        endif;
        ?>
    </div>
    <div class="diagonal-background cards-diag"></div>
</div>